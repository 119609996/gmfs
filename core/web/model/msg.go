package model

/**
 * 消息常量
 */
const (
	MSG_SUCCESS        = "1000" //正常消息
	MSG_ERROR          = "1001" //异常消息
	MSG_IMG_SAVE_ERROR = "1002" //保存失败
	MSG_IMG_READ_ERROR = "1003" //读取失败
	MSG_IMG_LESS_THAN  = "1004" //超过限制大小，默认小于80M
	MSG_IMG_ISNUDE     = "1005" //图片涉黄
	MSG_NOT_SUPPORT    = "1006" //不支持文件
)
