package main

import "giraffe/service/ink"

var (
	app *ink.App
)

func init() {
	// init new application.
	// it loads default config file "config.json". if not exist, use pre-defined config.
	app = ink.New()
}

// default handler, implement ink.Handler
func homeHandler(ctx *ink.Context) {
	ctx.Body = []byte("Hello ink !")
}

func main() {
	// only bind GET handler by homeHandler.
	// if other http method, return 404.
	app.Get("/", homeHandler)

	// run application.
	// it listens localhost:9000 in pre-defined config.
	app.Run()
}
