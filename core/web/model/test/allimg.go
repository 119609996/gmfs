package main

import (
	"fmt"
	"giraffe/core/web/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

func main() {
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("giraffe").C("img_classify")
	result := []model.ImgClassify{}
	err = c.Find(bson.M{"name": "photo"}).All(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("result:", result)
}
