package model

/**
 * 图片文件元信息
 */
type ImgMeta struct {
	Width  int //宽
	Height int //高
}

//width
func (this *ImgMeta) SetWidth(width int) {
	this.Width = width
}
func (this *ImgMeta) GetWidth() int {
	return this.Width
}

//height
func (this *ImgMeta) SetHeight(height int) {
	this.Height = height
}
func (this *ImgMeta) GetHeight() int {
	return this.Height
}
