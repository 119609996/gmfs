package redis

import (
	redigo "gopkg.in/redis.v3"
	"time"
)

var client = redigo.NewClient(&redigo.Options{
	Addr:     "192.168.1.252:6379",
	Password: "", // no password set
	DB:       0,  // use default DB
})

// Cache is a Redis Image Cache.
// https://gopkg.in/redis.v3
type Cache struct {
	Expire time.Duration // optional
}

func NewCache(expire int64) *Cache {
	return &Cache{time.Duration(expire) * time.Second}
}

/**
 * redis Get
 */
func (cache *Cache) Get(key string) (string, error) {
	value, err := client.Get(key).Result()
	return value, err
}

/**
 * redis Set
 */
func (cache *Cache) Set(key, value string) error {
	err := client.Set(key, value, cache.Expire).Err()
	return err
}

func (cache *Cache) SetByte(key string, value []byte) error {
	return cache.Set(key, string(value))
}
